/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import MapView from 'react-native-maps';

export default class hello extends Component {

  constructor(){
    super();
    this.state = {
      latitude: 37.78825,
      longitude: -122.4324,
      markers: []
    };
  }

  onPressButton() {
    navigator.geolocation.getCurrentPosition((data) => {
      this.setState(()=>{
        return {
          longitude: data.coords.longitude,
          latitude: data.coords.latitude,
          isRegionSet: true,
          markers: [{
            latlng: {
              longitude: data.coords.longitude, 
              latitude: data.coords.latitude
            },
            title: 'My Location'
          }]
        }
      });
    });
  }

  render() {
    const region = this.state.isRegionSet ? {
      longitude: this.state.longitude,
      latitude: this.state.latitude,
      latitudeDelta: 0.00922,
      longitudeDelta: 0.00421
    } : null;
    console.log(this.state.markers)
    return (
      <View style={styles.container}>
        <MapView
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          }}
          region={region}
          style={styles.map}
        >
          {this.state.markers.map(marker => (
              <MapView.Marker
                coordinate={marker.latlng}
                title={marker.title}
              />
            ))}
        </MapView>
        <Button
          onPress={this.onPressButton.bind(this)}
          title="Locate"
          color="#841584"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  map: {
    width: 300,
    height: 300
  }
});

AppRegistry.registerComponent('hello', () => hello);
