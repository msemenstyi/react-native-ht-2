/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  Image
} from 'react-native';

import axios from 'axios'

export default class List extends Component {

  constructor(){
    super();

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      ds: ds.cloneWithRows([]),

    };
  }

  componentDidMount(){
    axios.get('https://teamtreehouse.com/matthew.json')
      .then((response) => {
        this.setState({
          ds: this.state.ds.cloneWithRows(response.data.badges)
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          dataSource={this.state.ds}
          renderRow={(rowData) => 
            <View style={styles.row}>
              <Image source={{uri: rowData.icon_url}} style={styles.image} />
              <Text>
                {rowData.name}
              </Text>
            </View>
          }
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 10,
    backgroundColor: '#F6F6F6',
  },
  image: {
    width: 40, 
    height: 40,
    marginLeft: 20,
    marginRight: 20
  }
});

AppRegistry.registerComponent('hello', () => List);
